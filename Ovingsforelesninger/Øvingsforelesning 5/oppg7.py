# Lag en funksjon campus_statistikk som tar inn en liste på samme
# format som studenter.Funksjonen skal printe ut hvor mange studenter
# som tilhører Gjøvik, Ålesund og Trondheim.

studenter = [["Dwight", 23, "Gjøvik"], ["Michael", 19, "Ålesund"],
             ["Jim", 21, "Trondheim"], ["Pam", 19, "Trondheim"],
             ["Angela", 22, "Gjøvik"], ["Toby", 24, "Trondheim"],
             ["Kevin", 20, "Ålesund"]]

def campus_statistikk(liste): 
    studenter_gjovik = 0
    sudenter_aalesund = 0
    studenter_trondheim = 0

    for student in studenter:
        if student[2] == "Gjøvik":
            studenter_gjovik += 1
        elif student[2] == "Ålesund":
            studenter_aalesund += 1
        else:
            studenter_trondheim += 1

    print("Antall studenter fra Gjøvik campus:    ", studenter_gjovik)
    print("Antall studenter fra Ålesund campus:   ", studenter_aalesund)
    print("Antall studenter fra Trondheim campus: ", studenter_trondheim)