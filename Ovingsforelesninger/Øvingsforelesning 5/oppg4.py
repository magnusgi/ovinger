# Lag en funksjon under47(tider) som tar inn en liste med tider,
# og returnerer antall tider som er under 47.

# Kall funksjonen med lista til Karsten.

tider = [46.98, 47.13, 47.09, 46.58, 47.12]

def under47(liste_tider):
    antall_under47 = 0
    for tid in liste_tider:
        if tid < 47:
            antall_under47 += 1
    return antall_under47

print(under47(tider))


