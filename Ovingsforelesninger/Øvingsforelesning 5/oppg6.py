# Lag en funksjon slice_in_two som tar inn 
# en liste og returnerer første halvdel av listen.

def slice_in_two(liste):
    lengde = len(liste)
    halv = int(lengde/2)
    return liste[:halv]

print(slice_in_two([4,5,6,7,8,9,10]))