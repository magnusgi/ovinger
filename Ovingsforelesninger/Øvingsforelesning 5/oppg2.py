# a) Lag en handleliste med fem varer: 
#    havregryn, melk, bananer, squash og tomater.
handleliste = ["havregryn", "melk", "bananer", "squash", "tomater"]

# b) Bamsemums er på tilbud denne uka! 
#    Legg til bamsemums i handlelista.
handleliste.append("bamsemums")

# c) Du har ikke lyst på bananer likevel. 
#    Bytt ut bananer med epler.
handleliste[2] = "epler"

# d) Skriv ut  handlelista.
print(handleliste)
