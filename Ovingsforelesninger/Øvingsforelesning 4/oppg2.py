# a) Lag en funksjon som tar inn et fornavn og returnerer antall bokstaver i navnet
# b) Endre funksjonen slik at den nå kan ta inn et helt navn og returnere antall bokstaver i navnet
# c) Lag et program som kan sammenligne lengden av to navn ved  hjelp av denne funksjonen.
#    Programmet skal printe <Navn1> er lenger enn <Navn2> eller omvendt, avhengig av lengden på navnene.

# a)
def antall_bokstaver_a(navn):
    return len(navn)

# b)
def antall_bokstaver_b(navn):
    bokstaver = 0
    for element in navn:
        if element.isalpha():
            bokstaver += 1
    return bokstaver

# c)
navn1 = "Skrue McDuck"
navn2 = "Mikke Mus"

if antall_bokstaver_b(navn1) > antall_bokstaver_b(navn2):
    print(navn1, "har flere bokstaver enn", navn2)
elif antall_bokstaver_b(navn1) < antall_bokstaver_b(navn2)
    print(navn2, "har flere bokstaver enn", navn1)
else:
    print(navn1, "og", navn2, "har like mange bokstaver.")


            
