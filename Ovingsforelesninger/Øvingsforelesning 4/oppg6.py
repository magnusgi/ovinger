# Vi skal lage et enkelt terningspill. En runde i spillet går ut på at spilleren
# gjetter summen av antall øyne,før 5 terninger trilles. Hvis han gjetter riktig,
# får han 3 poeng. Hvis han er innenfor 5 fra riktig verdi, får han 1 poeng. 


import random

# Funksjon som returnerer et tilfeldig tall mellom lower og upper
def random_number(lower, upper):
    return random.randint(lower, upper)

# Funksjon som returnerer summen av antall øyne på n terninger
def sum_of_dices(n):
    total = 0
    for i in range(n):
        dice = random_number(1, 6)
        total += dice

    return total

# Funksjon som lar spilleren spille en runde
def one_round():
    guess = int(input("Gjett summen av antall øyne: "))
    dices = sum_of_dices(5)
    print("Sum av antall øyne:", dices)
    if guess == dices:
        points = 3
    elif abs(guess - dices) < 5:
        points = 1
    else:
        points = 0
    print("Antall poeng:", points)
    return points


# Lag en funksjon dice_game(). Spilleren skal trille 5 terninger 3 ganger.
# Før hvert kast,skal han gjette summen av antall øyne.
# Funksjonen skal til slutt printe den totale poengsummen.
# Hvis den totale poengsummen er 3 eller mer, skal funksjonen også printe “Gratulerer, du vant!”.
# Hvis ikke, skal funksjonen printe “Du tapte :/”

def dice_game():
    total_poeng = 0
    for i in range(3):
        poeng = one_round()
        total_poeng += poeng
        print()
    print("Totalt antall poeng: ", total_poeng)
    if total_poeng >= 3:
        print("Gratulerer, du vant!")
    else:
        print("Du tapte :/")
        
dice_game()
