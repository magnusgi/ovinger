# Lag et program som tar inn to tall,
# og skriver ut alle tallene mellom disse som er delelige på 3.

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et tall: "))

for i in range(tall1, tall2):
    if i % 3 == 0:
        print(i)