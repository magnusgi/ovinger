# Lag et program som gjør følgende
# Spør bruker om to tall (tall1 og tall2)
# Hvis tallene er like skal det skrives ut “Gratulerer, tallene er like”
# Hvis tall1 er større enn tall2, skal det skrives ut “<tall1> er større enn <tall2>”
# Hvis tall1 er mindre enn tall2, skal det skrives ut “<tall1> er mindre enn <tall2>”
# Til slutt skal det skrives ut “Takk for denne gang”

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et tall: "))

if tall1 == tall2:
    print("Gratulerer, tallene er like")
elif tall1 > tall2:
    print(tall1, "er større enn", tall2)
else:
    print(tall1, "er mindre enn", tall2)
print("Takk for denne gang")