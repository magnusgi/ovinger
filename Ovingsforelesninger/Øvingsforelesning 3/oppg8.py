# Grønn: Lag et program som printer ut et tilfeldig tall
# Gul:   Lag et program som printer ut ti tilfeldige tall
# Rød:   Print ut hvor mange oddetall som ble laget til slutt

import random
antall_oddetall = 0

for i in range(10):
    random_tall = random.randint(0,10)
    
    if random_tall % 2 != 0:
        antall_oddetall += 1
    
    print(random_tall)
    
print("Antall oddetall: ", antall_oddetall)