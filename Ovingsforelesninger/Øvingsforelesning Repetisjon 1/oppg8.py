# Lag en funksjon som ikke tar inn noen parametre.
# Funksjonen skal  stille brukeren et spørsmål.
# Hvis brukeren svarer feil, skal funksjonen fortsette å stille
# spørsmålet til brukeren har svart riktig.
# Funksjonen skal til slutt printe “Du svarte riktig”.

def matte_quiz():
    svar = int(input("Hva er 3 + 4? "))
    while svar != 7:
        svar = int(input("Feil svar. Hva er 3 + 4? "))
    print("Du svarte riktig")
    
matte_quiz()   # Eksempel på kjøring