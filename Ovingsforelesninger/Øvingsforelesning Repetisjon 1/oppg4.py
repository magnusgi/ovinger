# Lag en funksjon som tar inn tre parametre:
# innskudd, vekstfaktor og år, og returnerer sluttsummen.

def sluttsum(innskudd, vekstfaktor, aar):
    summ = innskudd * vekstfaktor**aar
    return summ

sum_etter_12_aar = sluttsum(20_000, 1.03, 12)   # Eksempel på kjøring, lagrer sluttsummen i en variabel
print(round(sum_etter_12_aar))