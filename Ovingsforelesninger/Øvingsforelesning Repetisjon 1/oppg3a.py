# Lag en funksjon som tar inn to tall som parametre,
# og returnerer differansen mellom de to tallene.

# Alternativ 1
def differanse(tall1, tall2):
    diff = tall1 - tall2
    diff_abs = abs(diff)
    return diff_abs

print(differanse(13, 20))   # Eksempel på kjøring

# Alternativ 2
def differanse2(tall1, tall2):
    return abs(tall1 - tall2)

print(differanse2(49, 20))   # Eksempel på kjøring