# Lag en funksjon som tar inn to tall som parametre,
# og printer “Differansen mellom <tall1> og <tall2> er <differanse>”

def differanse(tall1, tall2):
    diff = tall1 - tall2
    diff_abs = abs(diff)
    print("Differansen mellom", tall1, "og", tall2, "er", diff_abs)
    
differanse(40, 100)   # Eksempel på kjøring