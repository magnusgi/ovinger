# Lag en funksjon som tar inn to strenger som parametre. 
# Funksjonen skal returnere True hvis strengene er like lange,
# og False hvis de ikke er like lange.

# Alternativ 1
def like_lange(streng1, streng2):
    if len(streng1) == len(streng2):
        return True
    else:
        return False

print(like_lange("eple", "pære"))   # Eksempel på kjøring

# Alternativ 2
def like_lange2(streng1, streng2):
    return (len(streng1) == len(streng2))

print(like_lange2("eple", "banan"))   # Eksempel på kjøring
    

