# Hos Python EscapeRooms koster det 400kr per deltaker. 
# Python EscapeRooms gir også grupperabatter.

# 5 eller flere:  5% rabatt
# 10 eller flere: 15% rabatt
# 20 eller flere: 30 % rabatt

# Lag en funksjon som tar inn antall deltakere som parameter 
# og returnerer totalsummen som må betales.

def pris_python(antall):
    if antall >= 20:
        totalsum = 400 * antall * 0.7
    elif antall >= 10:
        totalsum = 400 * antall * 0.85
    elif antall >= 5:
        totalsum = 400 * antall * 0.95
    else:
        totalsum = 400 * antall
    return totalsum

print(pris_python(17))   # Eksempel på kjøring
