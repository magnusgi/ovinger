# Lag et program som ber bruker om å skrive inn et passord,
# og som sjekker om det er likt et passord som du har lagret i en variabel.
# Hvis passordet er likt, skal “Riktig passord” skrives ut til skjerm.

passord = "itgk123"
bruker_passord = input("Skriv inn passord: ")

if passord == bruker_passord:
    print("Riktig passord!")