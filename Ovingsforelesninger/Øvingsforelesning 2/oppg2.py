# Lag et program som gjør følgende
# 1) Spør bruker om tre tall, og lagrer disse som variabler
# 2) Sjekker om summen av de to første tallene er lik det siste tallet
# 3) Skriver ut resultatet til skjerm

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et tall: "))
tall3 = int(input("Skriv inn et tall: "))

er_like = (tall1 + tall2 == tall3)

print(tall1, "+", tall2, "=", tall3, ":", er_like)