# Lag et program som tar inn to tall. Programmet skal skrive ut
# ”<tall1> er større enn eller lik <tall2>” eller
# ”<tall1> er mindre enn <tall2>” avhengig av tallene.

tall1 = int(input("Skriv inn et tall: "))
tall2 = int(input("Skriv inn et tall: "))

if tall1 >= tall2:
    print(tall1, "er større enn eller lik", tall2)
    
else:
    print(tall1, "er mindre enn", tall2)