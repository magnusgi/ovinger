# Elias liker å trene. 
# På mandager, onsdager og fredager trener han styrke.
# På tirsdager og torsdager jogger han. 
# Lag et program der Elias kan skrive inn hvilken ukedag det er,
# og deretter få vite om han skal trene styrke eller jogge.

dag = input("Hvilken dag er det: ")

if (dag == "mandag" or dag == "onsdag" or dag == "fredag"):
    print("Idag skal du trene styrke")
else:
    print("Idag skal du jogge")